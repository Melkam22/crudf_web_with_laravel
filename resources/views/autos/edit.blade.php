


<a href="/autos">Back</a> <br>


<form action="/autos/{{$auto->id}}" method="POST">
    @csrf
    @method('PUT')
    <input type="text" name="car_name" value="{{$auto->car_name}}"> <br>
    <input type="text" name="car_model" value="{{$auto->car_model}}"> <br>
    <input type="integer" name="manufactured_year" value="{{$auto->manufactured_year}}"> <br>
    <button>Update</button>
</form>
