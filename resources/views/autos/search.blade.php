


{{--<a href="autos/create">Create New</a>--}}

<a href="/autos">Back</a>

@foreach($autos as $auto)
    <div style="width: 200px; border-bottom: 2px solid blue; padding-left: 20px;">
        <h4>{{$auto->car_name}}</h4>
        <h4>{{$auto->car_model}}</h4>
        <h4>{{$auto->manufactured_year}}</h4>

{{--                edit & delete functions--}}
        <div style="display: flex;">
            <a href="/autos/{{$auto->id}}/edit"><button style="width: 80px; background-color: green;">Edit</button></a>


            <form action="/autos/{{$auto->id}}" method="POST" style="margin-left: 5px;">
                @csrf
                @method('DELETE')
                <button style="width: 80px; background-color: red;">Delete</button>
            </form>
        </div>
    </div>
@endforeach
