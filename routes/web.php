<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AutosController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('/autos', AutosController::class);
Route::resource('/create', AutosController::class);
Route::resource('/edit', AutosController::class);
Route::resource('/delete', AutosController::class);

Route::get('/search', [AutosController::class, 'search']);
